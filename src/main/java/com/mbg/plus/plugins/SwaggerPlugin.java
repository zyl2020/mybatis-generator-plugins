package com.mbg.plus.plugins;

import org.apache.commons.lang3.StringUtils;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.TopLevelClass;

import java.util.List;

/**
 * Generator Swagger 插件
 *
 * @author ZhuYinglong
 * @date 2020/3/18
 */
public class SwaggerPlugin extends PluginAdapter {

    private int addAnnotationCount = 0;

    @Override
    public boolean validate(List<String> warnings) {
        return true;
    }

    @Override
    public boolean modelFieldGenerated(Field field, TopLevelClass topLevelClass, IntrospectedColumn introspectedColumn, IntrospectedTable introspectedTable, ModelClassType modelClassType) {
        if (StringUtils.isNotBlank(introspectedColumn.getRemarks())) {
            field.addAnnotation("@ApiModelProperty(value = \"" + introspectedColumn.getRemarks() + "\")");
            topLevelClass.addImportedType("io.swagger.annotations.ApiModelProperty");
        }
        if (StringUtils.isNotBlank(introspectedTable.getRemarks())) {
            if (addAnnotationCount == 0) {
                topLevelClass.addAnnotation("@ApiModel(value = \"" + introspectedTable.getRemarks() + "\")");
                topLevelClass.addImportedType("io.swagger.annotations.ApiModel");
            }
            addAnnotationCount++;
        }
        return true;
    }
}
